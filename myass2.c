/* Computes the power in total from antennae and available at various points,
then plots coverage maps.

Assignment 2 solution by Simeon Bain, student number 638697.
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<assert.h>

#define MAX_NUM_ANTENNAE 26
#define MAX_NUM_POINTS 100 
#define MAX_NUM_PLOTS 10
#define QUADRANT_ANGLE 90
#define CIRCLE_ANGLE 360
#define MY_PI 3.141592653589793
#define RAD_TO_DEG 180.0/MY_PI
#define OUTSIDE_RANGE -1 
#define CA 30.0
#define NO_BAR_UPPER 0.0000001
#define ONE_BAR_UPPER 0.00000032
#define TWO_BAR_UPPER 0.000001
#define THREE_BAR_UPPER 0.0000032
#define FOUR_BAR_UPPER 0.00001
#define EPS 0.00001

typedef struct {
	double x;
	double y;
} cartesian_t; 

typedef struct {
	char name;
	cartesian_t position; 
	double power;
	double orientation;
	double aperture;
} antenna_t;  

typedef struct {
	cartesian_t sw_cnr; 
	cartesian_t ne_cnr; 
	int pixels; 
} plot_t; 

void readin(antenna_t*, int*, cartesian_t*, int*, plot_t*, int*); 
void readantenna(antenna_t*); 
void readpoint(cartesian_t*); 
void readplot(plot_t*); 
void stage1(antenna_t*, int); 
void stage2(antenna_t*, int, cartesian_t*, int);
void stage3(antenna_t*, int, plot_t*, int); 
double calcsignal(antenna_t, cartesian_t);
double angletopoint(antenna_t, cartesian_t, double, double); 
void calcpixels(plot_t*, double*, int*, double*); 
void printpixel(antenna_t*, int, double, double, int, int, plot_t*); 
void skipstring(); 
int myround(double); 
int isequal(double, double); 

int 
main(int argc, char *argv[]) {
	
	antenna_t all_antennae[MAX_NUM_ANTENNAE]; 
	cartesian_t all_points[MAX_NUM_POINTS];
	plot_t all_plots[MAX_NUM_PLOTS]; 
	int num_antennae = 0, num_points = 0, num_plots = 0;
	
	/* read in from input */ 
	readin(all_antennae, &num_antennae, all_points, &num_points, 
		all_plots, &num_plots); 
	
	/* execute stages */ 
	stage1(all_antennae, num_antennae);  
	
	stage2(all_antennae, num_antennae, all_points, num_points); 
		
	stage3(all_antennae, num_antennae, all_plots, num_plots); 
	
	return 0;
}

void
readin(antenna_t* all_antennae, int* num_antennae, cartesian_t* all_points, 
	int* num_points, plot_t* all_plots, int* num_plots) {  

	int i; 
	
	/* read in number of input antennae */ 
	scanf("%d", num_antennae);
	
	assert(*num_antennae >= 0); 
	
	skipstring(); 
	
	/* read in each antenna */ 
	for (i=0; i<*num_antennae; i++) {
		readantenna(all_antennae+i); 
		skipstring();
	}
	
	/* read in number of input points */
	scanf("%d", num_points); 
	
	assert(*num_points >= 0); 
	
	skipstring(); 
	
	/* read in each point */ 
	for (i=0; i<*num_points; i++) {
		readpoint(all_points+i); 
		skipstring();
	}
	
	/* read in number of input plots */
	scanf("%d", num_plots); 
	
	assert(*num_plots >= 0); 
	
	skipstring(); 
	
	/* read in each plot */ 
	for (i=0; i<*num_plots; i++) {
		readplot(all_plots+i); 
		skipstring();
	}
}

void
readantenna(antenna_t* antenna) {
	
	scanf("%c %lf %lf %lf %lf %lf",
		&antenna->name,
		&antenna->position.x,
		&antenna->position.y,
		&antenna->power,
		&antenna->orientation,
		&antenna->aperture); 	
}

void
readpoint(cartesian_t* point) {
	
	scanf("%lf %lf",
		&point->x,
		&point->y); 
}

void
readplot(plot_t* plot) {
	
	scanf("%lf %lf %lf %lf %d", 
		&plot->sw_cnr.x,
		&plot->sw_cnr.y,
		&plot->ne_cnr.x,
		&plot->ne_cnr.y,
		&plot->pixels); 
}

void
stage1(antenna_t* all_antennae, int num_antennae) {
	
	double total_power = 0; 
	int i; 
	
	/* calculate total power */ 
	for (i=0; i<num_antennae; i++) {
		total_power += all_antennae[i].power;
	}
	
	/* print Stage 1 output */
	printf("Stage 1\n");
	printf("-------\n");
	printf("number of antennae: %2d\n", num_antennae); 
	printf("total power output: %2.1f Watts\n", total_power); 
}

void 
stage2(antenna_t* all_antennae, int num_antennae, cartesian_t* all_points, 
	int num_points) {

	int i, j; 
	double signal_power; 
	
	/* print heading */ 
	printf("Stage 2\n");
	printf("-------\n"); 
	printf("              "); 
	for (i=0; i<num_antennae; i++) {
		printf("    %c    ", all_antennae[i].name); 
	}
	printf("\n"); 
	
	/* calculate and print power  */ 
	for (i=0; i<num_points; i++) {
		printf("at (%2.1f,%2.1f): ", all_points[i].x, all_points[i].y);
		
		for (j=0; j<num_antennae; j++) {
			/* calculate power from each antenna at point */
			signal_power = 
			calcsignal(all_antennae[j], all_points[i]); 
			
			/* check if point outside range */ 
			if (signal_power < 0) {
				printf("   ---   "); 
			} else {
				printf(" %2.1e ", signal_power); 
			}
		}
		printf("Watts\n"); 
	}
}

void
stage3(antenna_t* all_antennae, int num_antennae, plot_t* all_plots, 
	int num_plots) {

	int i, j, k, y_pixels;
	double y_interval, x_interval;
	
	/* print heading */ 
	printf("Stage 3\n"); 
	printf("-------\n");
	
	for (i=0; i<num_plots; i++) {
		
		calcpixels(all_plots+i, &y_interval, &y_pixels, &x_interval);  
		
		/* iterating along columns and then down rows */ 
		for (j=y_pixels-1; j>=0; j--) {
			
			for (k=0; k<all_plots[i].pixels; k++) {
				
				printpixel(all_antennae, num_antennae,
					x_interval, y_interval, j, k, 
					all_plots+i); 
			}
			printf("\n"); 
		}
		printf("\n"); 
	}
}

double
calcsignal(antenna_t antenna, cartesian_t point) {
	
	double x_distance, y_distance, angle, negangle, posangle, 
	signal_power, distance;
	
	x_distance = point.x - antenna.position.x; 
	y_distance = point.y - antenna.position.y;
	
	distance = sqrt(x_distance*x_distance + y_distance*y_distance); 
	
	/* full power if point is at the antenna location */ 
	if (isequal(x_distance,0) && isequal(y_distance,0)) {
		return antenna.power; 
	}
	
	/* calculate angle from antenna orientation */ 
	angle = angletopoint(antenna, point, x_distance, y_distance) - 
	antenna.orientation; 
	/* accounting for angles <0 and >360 */
	negangle = angle - CIRCLE_ANGLE; 
	posangle = CIRCLE_ANGLE + angle; 
	
	/* check if outside range */ 
	if ((angle > antenna.aperture/2.0 || -angle > antenna.aperture/2.0)
		&& (negangle > antenna.aperture/2.0 || 
			-negangle > antenna.aperture/2.0)
		&& (posangle > antenna.aperture/2.0 || 
			posangle > antenna.aperture/2.0)) {
			return OUTSIDE_RANGE; 
	}
	
	/* if not, find power */ 
	signal_power = (CIRCLE_ANGLE*(antenna.power))/
		(antenna.aperture*pow((1.0 + CA*distance),4.0)); 
	
	return signal_power; 
}

double
angletopoint(antenna_t antenna, cartesian_t point, double x_distance, 
	double y_distance) {	

	/* return the angle of the point with the antenna as the origin */ 
	
	/* first check for divide by zero possibilities */ 
	if (isequal(x_distance, 0)) {
		if (y_distance > 0) {
			return 0; /* orientation is zero degrees */ 
		} else {
			return 2*QUADRANT_ANGLE; 
		}
	}
	if (isequal(y_distance, 0)) {
		if (x_distance > 0) {
			return QUADRANT_ANGLE; 
		} else {
			return 3*QUADRANT_ANGLE; 
		}
	} 
	
	/* then use arctan to find angle */ 
	if (x_distance >= 0 && y_distance >= 0) {
		/* in quadrant 1 */ 
		return atan(x_distance/y_distance)*RAD_TO_DEG; 
	} else if (x_distance >= 0 && y_distance < 0) {
		/* in quadrant 2 */ 
		return 
		QUADRANT_ANGLE + atan(-y_distance/x_distance)*RAD_TO_DEG; 
	} else if (x_distance < 0 && y_distance < 0) {
		/* in quadrant 3 */ 
		return
		3*QUADRANT_ANGLE - atan(y_distance/x_distance)*RAD_TO_DEG; 
	} else {
		/* in quadrant 4 */ 
		return 
		3*QUADRANT_ANGLE + atan(y_distance/-x_distance)*RAD_TO_DEG; 
	}
}

void
calcpixels(plot_t* plot, double* y_interval, int* y_pixels, 
	double* x_interval) {
	
	/* first calculate the interval between horizontal gridlines */
	*x_interval = (plot->ne_cnr.x - plot->sw_cnr.x)/
		(plot->pixels - 1);  
		
	/* vertical gridline intervals are approximately double */ 
	*y_interval = 2*(*x_interval); 
	
	/* round the number of vertical pixels required to nearest integer */
	*y_pixels = myround((1+(plot->ne_cnr.y - plot->sw_cnr.y)/
		*y_interval)); 
	
	/* then recalculate the vertical gridline interval */
	*y_interval = (plot->ne_cnr.y - plot->sw_cnr.y)/(*y_pixels-1);
}

void
printpixel(antenna_t* all_antennae, int num_antennae, double x_interval, 
	double y_interval, int row, int column, plot_t* plot) {

	int i; 
	double max_signal = OUTSIDE_RANGE, signal;
	cartesian_t position; 
	
	for (i=0; i<num_antennae; i++) {
		/* position of pixel */
		position.x = column*x_interval + plot->sw_cnr.x; 
		position.y = row*y_interval + plot->sw_cnr.y;
		
		/* calclulate the greatest signal here */
		signal = calcsignal(all_antennae[i], position); 
		
		if (signal > max_signal) {
			max_signal = signal; 
		} 
	}
	
	/* print number of bars according to signal strength */ 
	if (max_signal < NO_BAR_UPPER) {
		printf("."); 
	} else if (max_signal < ONE_BAR_UPPER) {
		printf("1"); 
	} else if (max_signal < TWO_BAR_UPPER) {
		printf("2"); 
	} else if (max_signal < THREE_BAR_UPPER) {
		printf("3"); 
	} else if (max_signal < FOUR_BAR_UPPER) {
		printf("4"); 
	} else {
		printf("5"); 
	}
}

void
skipstring() {
	
	char c;
	
	while ((c = getchar())) {
		/* keep consuming characters until new line character */ 
		if (strncmp(&c,"\n",1) == 0) {
			break; 
		}
	}
}

int
myround(double x) {
	
	if (x>=0) {
		return (int)(x+0.5);
        } else {
        	return (int)(x-0.5);
        }
}

int
isequal(double x, double y) {
	
	if ((x - y < EPS) && (y - x < EPS)) {
		return 1; 
	}
	
	return 0; 	
}

/* programming is fun! */ 

/* =====================================================================
   Program written by Simeon Bain, student number 638697, as a 
   solution for Engineering Computatioon assignment 2.
   
   Prepared May 2014
   ================================================================== */
