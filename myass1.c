/* Compute arrangement and wastage of components being cut from coils   
*/

#include<stdio.h>
#include<stdlib.h>
#include<assert.h> 

#define INITIAL_COIL 1
#define COIL_WIDTH 3.5
#define COIL_LENGTH 24
#define INITIAL_X 0
#define INITIAL_Y 0
#define MAX_COMPONENTS 1000
#define TRUE 1
#define FALSE 0

void stage1and2(double*,double*,int*,int);
void stage3(double*,double*,int*,int);
void checkinput(double,double); 
void flip(double*,double*); 
void stage_compute(double*,double*,int*,int); 
double calc_pos(double,double*,int*,int*,double*);
void bubblesort(double*,double*,int*,int);


int 
main(int argc, char *argv[]) {
	double X[MAX_COMPONENTS], Y[MAX_COMPONENTS];
	int i=0, N[MAX_COMPONENTS];
	
	/* read length and width into arrays */
	while (scanf("%lf %lf",X+i,Y+i)==2) { 
		
		/* check for input error */ 
		checkinput(*(X+i),*(Y+i)); 
		
		/* flip component if needed */
		flip(X+i,Y+i); 
		
		/* fill array that stores component number */
		*(N+i) = i+1; 
		
		i++;
	}
	
	assert(i<=1000); 
	
	/* EXECUTE STAGE 1 + 2 */ 
	stage1and2(X,Y,N,i);
	
	/* EXECUTE STAGE 3 */ 
	stage3(X,Y,N,i); 
	
	return 0;
}

void
stage1and2(double* X, double* Y, int* N, int i) {
	
	/* print heading */ 
	printf("Stage 1+2\n");
	printf("-------\n");
	
	/* pass arrays into function to execute output */ 
	stage_compute(X,Y,N,i); 
}

void
stage3(double* X, double* Y, int* N, int i) {
	int j, k, N2[MAX_COMPONENTS];
	double sumlength=0, X2[MAX_COMPONENTS], Y2[MAX_COMPONENTS];
	
	/* sort arrays from longest to shortest component */
	bubblesort(X,Y,N,i); 
	
	/* iterating to sort into new arrays in correct order */
	for (j=0;j<i;j++) {
		for (k=0;k<i;k++) { 
			if (*(X+k)+sumlength <= COIL_LENGTH && *(X+k) >= 0) {
				/* component is the largest that fits on coil */
				/* put into new sorted array */ 
				*(X2+j) = *(X+k);
				*(Y2+j) = *(Y+k);
				*(N2+j) = *(N+k);
				sumlength += *(X+k); 
				
				/* don't use this component again */
				*(X+k) = -1;  
				break; 
			}
			if (k==i-1) {
				/* found no component to fit on current coil */
				/* start sorting again for a new coil */
				sumlength=0; 
				j--;
			}
		} 
	}
	
	/* print heading */
	printf("Stage 3\n");
	printf("-------\n");
	
	/* pass ordered arrays into function to execute output */ 
	stage_compute(X2,Y2,N2,i); 
}

void
checkinput(double length, double width) {
	/* check if longer measurement input first */
	if (length<width) {
		printf("Error: longer measurement must be input first\n");
		exit(EXIT_FAILURE); 
	}
	
	/* check if component width is too large */ 
	if (width>COIL_WIDTH) {
		printf("Error: component width greater than coil width\n"); 
		exit(EXIT_FAILURE); 
	}
	
	/* check if component length is too large */ 
	if (length>COIL_LENGTH) {
		printf("Error: component length greater than coil length\n"); 
		exit(EXIT_FAILURE);
	}
	
	/* check for negative inputs */
	if (width<=0 || length<=0) {
		printf("Error: input must be positive\n"); 
		exit(EXIT_FAILURE);
	}
}

void
flip(double* length, double* width) {
	double temp;
	if (*length<=COIL_WIDTH) {
		/* should be flipped so swap length and width */
		temp = *length;
		*length = *width;
		*width = temp; 
	}
}

void
stage_compute(double* X, double* Y, int* N, int i) {
	double length, width, start, xpos=INITIAL_X, ypos=INITIAL_Y; 
	double sumwaste=0,intwaste=0,endwaste=0; 
	int j, coil=INITIAL_COIL, changecoil=FALSE, compnum;
	
	/* loop for each component */ 
	for (j=0;j<i;j++) {
		
		length = *(X+j);
		width = *(Y+j); 
		compnum = *(N+j); 
		
		/* calculate start position and coil */
		start = calc_pos(length,&xpos,&coil,&changecoil,&endwaste);
		xpos += length; 
		
		/* print wastage when coil used up */
		if (changecoil==TRUE) {
			printf("coil %2d, ",coil-1);
			printf("internal wastage %4.1f m^2, ",intwaste);
			printf("end wastage %4.1f m^2\n", endwaste); 
			/* add to total wastage */
			sumwaste += intwaste+endwaste; 
			/* reset */ 
			intwaste = 0;
			changecoil = FALSE;
		}
		
		/* add to internal wastage */ 
		intwaste += (COIL_WIDTH-width)*length;
		
		/* print output line for each component */
		printf("component %2d, (%4.1f,%4.1f), ",compnum,length,width);
		printf("starting (%4.1f,%4.1f) on coil %2d\n",start,ypos,coil);
	}
	
	/* calculate endwaste for last coil */
	endwaste = (COIL_LENGTH-xpos)*COIL_WIDTH; 
	
	/* print waste for last coil */
	printf("coil %2d, internal wastage %4.1f m^2, ",coil,intwaste);
	printf("end wastage %4.1f m^2\n", endwaste); 
	
	/* add to total waste and print total waste */ 
	sumwaste += intwaste; 
	printf("overall, total wastage %4.1f m^2\n\n", sumwaste); 
}

double
calc_pos(double length, double* xpos, int*coil, int* changecoil, 
	double* endwaste) {

	if (*xpos+length>COIL_LENGTH) {
		/* does not fit on current coil */
		/* calculate end waste for the coil */
		*endwaste = (COIL_LENGTH-*xpos)*COIL_WIDTH; 
		
		/*start a new coil */ 
		*xpos=0; 
		(*coil)++;
		*changecoil = TRUE; 
	}
	return *xpos; 
}

void
bubblesort(double* X, double* Y, int* N, int i) {
	int j, isswap=1;
	double temp; 
	
	/* sort arrays from largest to smallest x-component */
	/* keep trying until no swaps are made */
	while (isswap==TRUE) {
		isswap = FALSE;
		for (j=0;j<i-1;j++) { 
			if (*(X+j)<*(X+j+1)){
				/* elements out of order, swap */ 
				temp = *(X+j); 
				*(X+j) = *(X+j+1);
				*(X+j+1) = temp; 
				/* swap y-component */
				temp = *(Y+j); 
				*(Y+j) = *(Y+j+1);
				*(Y+j+1) = temp; 
				/* swap component numbers */
				temp = *(N+j); 
				*(N+j) = *(N+j+1);
				*(N+j+1) = temp; 
				
				isswap = TRUE; 
			}	
		}            
	}
}

/* programming is fun! */ 

/* =====================================================================
   Program written by Simeon Bain, student number 638697, as a 
   solution for Engineering Computatioon assignment 1.
   
   Prepared April 2014
   ================================================================== */
